/****************************************************************************
*
* XMLSceneParser.h
* A simple XML parser for scene information.
*
* Original code copyright (c) 2012 Ellen Price <eprice@caltech.edu>
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#ifndef __XMLSCENEPARSER_H__
#define __XMLSCENEPARSER_H__

#include <stdio.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "World.h"
#include "Entity.h"
#include "Sphere.h"
#include "Light.h"
#include "PointLight.h"
#include "structs.h"

class XMLSceneParser
{
public:
    XMLSceneParser();
    ~XMLSceneParser();

    int load_scene(World *world, char *file);

private:
    Color *parse_color(xmlChar *str);
    float parse_float(xmlChar *str);
    Material *parse_material(xmlNodePtr node);
    Light *parse_pointlight(xmlNodePtr node);
    Vertex *parse_vertex(xmlChar *str);
    Viewport *parse_viewport(xmlNodePtr node);
};

#endif
