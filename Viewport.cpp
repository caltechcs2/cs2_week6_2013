#include "Viewport.h"

Viewport::Viewport(Vertex *tl, Vertex *tr, Vertex *bl)
{
    float dx, dy, dz;

    topleft = tl;
    topright = tr;
    bottomleft = bl;

    dx = bottomleft->x - topleft->x;
    dy = bottomleft->y - topleft->y;
    dz = bottomleft->z - topleft->z;

    height = sqrtf(dx * dx + dy * dy + dz * dz);

    dx = topright->x - topleft->x;
    dy = topright->y - topleft->y;
    dz = topright->z - topleft->z;

    width = sqrtf(dx * dx + dy * dy + dz * dz);
}

Viewport::~Viewport()
{
    delete topleft;
    delete topright;
    delete bottomleft;
}

int Viewport::get_height()
{
    return (int) height;
}

int Viewport::get_width()
{
    return (int) width;
}

void Viewport::set_bitmap(BITMAP *bmp)
{
    bitmap = bmp;
}

BITMAP *Viewport::get_bitmap()
{
    return bitmap;
}

void Viewport::map_pixel_to_vertex(int x, int y, Vertex **v)
{
    float a, b, c;

    *v = new Vertex();

    a = (topright->x - topleft->x);
    b = (topright->y - topleft->y);
    c = (topright->z - topleft->z);

    (*v)->x = topleft->x + ((float) x / width) * a;
    (*v)->y = topleft->y + ((float) x / width) * b;
    (*v)->z = topleft->z + ((float) x / width) * c;

    a = (bottomleft->x - topleft->x);
    b = (bottomleft->y - topleft->y);
    c = (bottomleft->z - topleft->z);

    (*v)->x += ((float) y / height) * a;
    (*v)->y += ((float) y / height) * b;
    (*v)->z += ((float) y / height) * c;
}

void Viewport::color_pixel(int x, int y, Color *color)
{
    if (color->r > 1.0)
        color->r = 1.0;

    if (color->g > 1.0)
        color->g = 1.0;

    if (color->b > 1.0)
        color->b = 1.0;

    _putpixel32(bitmap, x, y, makecol((int) (color->r * 255.0),
        (int) (color->g * 255.0), (int) (color->b * 255.0)));

    count++;
}
