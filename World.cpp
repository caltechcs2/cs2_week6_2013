#include "World.h"

/*
 * Initializes the world.
 */
World::World()
{
    eye = NULL;
    viewport = NULL;
}
/*
 * Deinitializes the world and frees all memory.
 */
World::~World()
{
    Entity *e;
    Light *l;

    /* Free all Entities. */
    while (!entities.empty())
    {
        e = entities.back();
        entities.pop_back();
        delete e;
    }

    /* Free all Lights. */
    while (!lights.empty())
    {
        l = lights.back();
        lights.pop_back();
        delete l;
    }

    /* Free the viewport and eye. */
    if (eye)
        delete eye;

    if (viewport)
        delete viewport;
}

void World::set_eye(Vertex *e)
{
    eye = e;
}

void World::set_viewport(Viewport *v)
{
    viewport = v;
}

Vertex *World::get_eye()
{
    return eye;
}

Viewport *World::get_viewport()
{
    return viewport;
}

void World::add_entity(Entity *e)
{
    entities.push_back(e);
}

void World::add_light(Light *l)
{
    lights.push_back(l);
}

vector<Entity *> World::get_entity_vector()
{
    return entities;
}

vector<Light *> World::get_light_vector()
{
    return lights;
}

void World::get_eye_viewport_ray(int x, int y, Ray **ray)
{
    Vertex *origin = eye, *direction;

    viewport->map_pixel_to_vertex(x, y, &direction);
    *ray = new Ray(new Vertex(origin), new Vertex(direction));

    delete direction;
}

int World::get_viewport_height()
{
    return viewport->get_height();
}

int World::get_viewport_width()
{
    return viewport->get_width();
}
