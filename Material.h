#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include "structs.h"

class Material
{
public:
    Material(Color *c, float reflect, float refract);
    ~Material();

    Color *get_color();
    float get_reflectivity();
    float get_refractivity();

private:
    Color *color;
    float reflectivity, refractivity;
};

#endif
