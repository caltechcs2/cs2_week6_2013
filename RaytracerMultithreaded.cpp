#include "RaytracerMultithreaded.h"

/* This constant can be tuned to meet specific project needs; the only
 * requirement is that it be small compared to the scale of the
 * Entities. It is necessary to prevent shadow rounding errors. */
#define EPSILON     (5.0)

RaytracerMultithreaded::RaytracerMultithreaded(World *w, Shader *s)
{
    world = w;
    shader = s;
}

RaytracerMultithreaded::~RaytracerMultithreaded()
{

}

void RaytracerMultithreaded::run()
{
	
}

/*
 * Returns true if `ray` intersects an Entity in the World, false
 * otherwise. If there is one or more intersection, `closest_intersect`
 * and `closest_entity` will contain pointers to the point of
 * intersection and the entity itself, respectively.
 */
bool RaytracerMultithreaded::get_closest_entity_intersection(Ray *ray,
Ray **normal, Entity **entity)
{
    unsigned int i;
    Ray *temp;
    vector<Entity *> entities = world->get_entity_vector();

    *normal = NULL;
    *entity = NULL;

    for (i = 0; i < entities.size(); i++)
    {
        if (entities[i]->does_intersect(ray, &temp))
        {
            if (!(*normal))
            {
                *normal = temp;
                *entity = entities[i];
            }
            else if (Vertex::dist_squared(ray->get_origin(), temp->get_origin()) <
                Vertex::dist_squared(ray->get_origin(), (*normal)->get_origin()))
            {
                delete *normal;
                *normal = temp;
                *entity = entities[i];
            }
            else
            {
                delete temp;
            }
        }
    }

    if (*normal)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
 * Returns true if `ray`, which defines a ray from `entity` to `light`,
 * is blocked by any Entity in the World, false otherwise. `entity` is
 * required because shadow rounding error correction is generally only
 * necessary for the Entity of origin, so we don't waste time with it
 * otherwise.
 */
bool RaytracerMultithreaded::light_blocked(Ray *ray, Light *light,
Entity *entity)
{
    unsigned int i;
    Ray *normal;
    Vertex *o, *d;
    vector<Entity *> entities = world->get_entity_vector();

    for (i = 0; i < entities.size(); i++)
    {
        if (entities[i] == entity)
        {
            /* We'll need to twitch the origin of the ray along the
             * Entity normal vector. For now we'll just add the normal
             * vector, scaled by EPSILON, to the original ray's origin. */
            o = ray->get_origin();
            d = ray->get_displacement();

            o->x += d->x * EPSILON;
            o->y += d->y * EPSILON;
            o->z += d->z * EPSILON;
        }

        if (entities[i]->does_intersect(ray, &normal))
        {
            delete normal;
            return true;
        }
    }

    return false;
}
