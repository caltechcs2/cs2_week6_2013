#ifndef __SPHERE_H__
#define __SPHERE_H__

#include <math.h>
#include "Entity.h"
#include "Ray.h"
#include "structs.h"

using namespace std;

class Sphere : protected Entity
{
public:
    Sphere(Vertex *o, float r, Material *mat);
    ~Sphere();

    bool does_intersect(Ray *ray, Ray **normal);

private:
    float radius;

    void transform_ray_to_local_space(Ray *r, Ray **dest);
    void transform_normal_to_world_space(Vertex *n);
    void transform_intersection_point_to_world_space(Vertex *i);
};

#endif
