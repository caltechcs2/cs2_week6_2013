#ifndef __RAYTRACERMULTITHREADED_H__
#define __RAYTRACERMULTITHREADED_H__

#include <stdio.h>
#include <math.h>
#include "RaytracerBase.h"
#include "Shader.h"
#include "World.h"
#include "Thread.h"

using namespace std;

class RaytracerMultithreaded : RaytracerBase
{
public:
    RaytracerMultithreaded(World *w, Shader *s);
    ~RaytracerMultithreaded();

    void run();

    bool get_closest_entity_intersection(Ray *ray, Ray **normal, Entity **entity);
    bool light_blocked(Ray *ray, Light *light, Entity *entity);

private:
    World *world;
    Shader *shader;
};

#endif
