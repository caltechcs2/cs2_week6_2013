#include "Sphere.h"

Sphere::Sphere(Vertex *o, float r, Material *mat)
{
    radius = r;
    material = mat;

    if ((o->x != 0.0) || (o->y != 0.0) || (o->z != 0.0))
        t = new TranslationMatrix(o);

    if (radius != 1.0)
        s = new ScalingMatrix(radius, radius, radius);

    delete o;
}

Sphere::~Sphere()
{
    
}

bool Sphere::does_intersect(Ray *ray, Ray **normal)
{
    float d, d1, d2;
    Ray *r;
    Vertex *ro, *rd, *n, *i;

    transform_ray_to_local_space(ray, &r);
    ro = r->get_origin();
    rd = r->get_displacement();

    float x0 = ro->x, y0 = ro->y, z0 = ro->z;
    float dx = rd->x, dy = rd->y, dz = rd->z;

    delete r;

    float a = dx * dx + dy * dy + dz * dz;
    float b = 2.0 * (dx * x0 + dy * y0 + dz * z0);
    float c = x0 * x0 + y0 * y0 + z0 * z0 - 1.0;

    float delta = b * b - 4.0 * a * c;

    if (delta < 0)
    {
        return false;
    }
    else if (delta == 0.0)
    {
        d = -b / (2.0 * a);
    }
    else
    {
        d1 = (-b + sqrtf(delta)) / (2.0 * a);
        d2 = (-b - sqrtf(delta)) / (2.0 * a);

        if ((d1 < 0) && (d2 < 0))
        {
            return false;
        }
        else if (d1 < 0)
        {
            d = d2;
        }
        else if (d2 < 0)
        {
            d = d1;
        }
        else
        {
            d = d1 < d2 ? d1 : d2;
        }
    }

    n = new Vertex(x0 + d * dx, y0 + d * dy, z0 + d * dz);
    transform_normal_to_world_space(n);

    i = new Vertex(x0 + d * dx, y0 + d * dy, z0 + d * dz);
    transform_intersection_point_to_world_space(i);

    *normal = new Ray(i, n);

    return true;
}

void Sphere::transform_ray_to_local_space(Ray *r, Ray **dest)
{
    Vertex *o = new Vertex(r->get_origin());
    Vertex *d = new Vertex(r->get_displacement());

    /* O' = S^-1 * R^-1 * T^-1 * O */
    if (t)
        t->multiply_inverse_by_vertex(o, o);

    if (s)
        s->multiply_inverse_by_vertex(o, o);

    /* D' = S^-1 * R^-1 * D */
    if (s)
        s->multiply_inverse_by_vertex(d, d);

    *dest = new Ray(o, d);
}

void Sphere::transform_normal_to_world_space(Vertex *n)
{
    /* N = R * S^-1 * N' */
    if (s)
        s->multiply_inverse_by_vertex(n, n);
}

void Sphere::transform_intersection_point_to_world_space(Vertex *i)
{
    /* P = T * R * S * P' */
    if (s)
        s->multiply_by_vertex(i, i);

    if (t)
        t->multiply_by_vertex(i, i);
}
