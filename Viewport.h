#ifndef __VIEWPORT_H__
#define __VIEWPORT_H__

#include <allegro.h>
#include <math.h>
#include "structs.h"
#include "Thread.h"

class Viewport
{
public:
    Viewport(Vertex *tl, Vertex *tr, Vertex *bl);
    ~Viewport();

    int get_height();
    int get_width();

    void set_bitmap(BITMAP *bmp);
    BITMAP *get_bitmap();

    void map_pixel_to_vertex(int x, int y, Vertex **v);
    void color_pixel(int x, int y, Color *color);

private:
    Vertex *topleft, *topright, *bottomleft;
    BITMAP *bitmap;
    float height, width;
    unsigned long count;
};

#endif
