#ifndef __RAYTRACERBASE_H__
#define __RAYTRACERBASE_H__

class RaytracerBase
{
public:
    RaytracerBase() { }
    virtual ~RaytracerBase() { }

    virtual void run() = 0;
};

#endif
