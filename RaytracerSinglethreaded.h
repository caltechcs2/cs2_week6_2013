#ifndef __RAYTRACERSINGLETHREADED_H__
#define __RAYTRACERSINGLETHREADED_H__

#include <stdio.h>
#include <math.h>
#include "RaytracerBase.h"
#include "Shader.h"
#include "World.h"

using namespace std;

class RaytracerSinglethreaded : RaytracerBase
{
public:
    RaytracerSinglethreaded(World *w, Shader *s);
    ~RaytracerSinglethreaded();

    void run();

    void trace(Ray *ray, Color **color);
    bool get_closest_entity_intersection(Ray *ray, Ray **normal, Entity **entity);
    bool light_blocked(Ray *ray, Light *light, Entity *entity);

private:
    World *world;
    Shader *shader;
};

#endif
