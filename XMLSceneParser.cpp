/****************************************************************************
*
* XMLSceneParser.cpp
* A simple XML parser for scene information.
*
* Original code copyright (c) 2012 Ellen Price <eprice@caltech.edu>
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#include "XMLSceneParser.h"

XMLSceneParser::XMLSceneParser()
{

}

XMLSceneParser::~XMLSceneParser()
{

}

int XMLSceneParser::load_scene(World *world, char *file)
{
    xmlDocPtr doc;
    xmlNodePtr cur;
    xmlChar *prop[10];

    float f1;
    Light *l;
    Material *m;
    Vertex *v1;
    Viewport *vp;

    /* Open the file for parsing. */
    doc = xmlParseFile(file);

    if (doc == NULL)
        return -1;

    /* Get the root element (scene). */
    cur = xmlDocGetRootElement(doc);

    if (cur == NULL)
    {
        xmlFreeDoc(doc);
        return -1;
    }

    if (xmlStrcmp(cur->name, (const xmlChar *) "scene") != 0)
    {
        xmlFreeDoc(doc);
        return -1;
    }

    /* Set the Viewport and Eye, and add the Entities and Lights to
     * the scene. */
    cur = cur->xmlChildrenNode;

    while (cur != NULL)
    {
        if (xmlStrcmp(cur->name, (const xmlChar *) "eye") == 0)
        {
            prop[0] = xmlGetProp(cur, (const xmlChar *) "position");
            v1 = parse_vertex(prop[0]);
            xmlFree(prop[0]);

            world->set_eye(v1);
        }
        else if (xmlStrcmp(cur->name, (const xmlChar *) "viewport") == 0)
        {
            if ((vp = parse_viewport(cur->xmlChildrenNode)) == NULL)
            {
                return -1;
            }
            else
            {
                world->set_viewport(vp);
            }
        }
        else if (xmlStrcmp(cur->name, (const xmlChar *) "sphere") == 0)
        {
            prop[0] = xmlGetProp(cur, (const xmlChar *) "origin");
            prop[1] = xmlGetProp(cur, (const xmlChar *) "radius");

            v1 = parse_vertex(prop[0]);
            f1 = parse_float(prop[1]);
            m = parse_material(cur->xmlChildrenNode);

            xmlFree(prop[0]);
            xmlFree(prop[1]);

            world->add_entity((Entity *) new Sphere(v1, f1, m));
        }
        else if (xmlStrcmp(cur->name, (const xmlChar *) "light") == 0)
        {
            prop[0] = xmlGetProp(cur, (const xmlChar *) "type");

            if (xmlStrcmp(prop[0], (const xmlChar *) "point") == 0)
            {
                l = parse_pointlight(cur);
            }

            xmlFree(prop[0]);
            world->add_light(l);
        }

        cur = cur->next;
    }

    xmlFreeDoc(doc);
    return 0;
}

/*
 * Parses an xmlChar string of the form "(%f, %f, %f)" to a new
 * corresponding Color. If a NULL string is supplied, it returns a
 * default (white).
 */
Color *XMLSceneParser::parse_color(xmlChar *str)
{
    float r, g, b;

    if (str == NULL)
    {
        return new Color(1.0, 1.0, 1.0);
    }
    else
    {
        sscanf((char *) str, "(%f, %f, %f)", &r, &g, &b);
        return new Color(r, g, b);
    }
}

/*
 * Parses an xmlChar string to a float. If a NULL string is supplied, it
 * returns a default (zero).
 */
float XMLSceneParser::parse_float(xmlChar *str)
{
    float f;

    if (str == NULL)
    {
        return 0.0;
    }
    else
    {
        sscanf((char *) str, "%f", &f);
        return f;
    }
}

/*
 * Parses a material node, returning a valid Material. Note: This function
 * will always return a valid Material. If an error occurs, it will
 * return a default (white matte) material.
 */
Material *XMLSceneParser::parse_material(xmlNodePtr node)
{
    xmlNodePtr cur = node;
    xmlChar *prop[10];

    Color *c;
    float f1, f2;

    while (cur != NULL)
    {
        if (xmlStrcmp(cur->name, (const xmlChar *) "material") == 0)
        {
            prop[0] = xmlGetProp(cur, (const xmlChar *) "color");
            prop[1] = xmlGetProp(cur, (const xmlChar *) "reflectivity");
            prop[2] = xmlGetProp(cur, (const xmlChar *) "refractivity");

            c = parse_color(prop[0]);
            f1 = parse_float(prop[1]);
            f2 = parse_float(prop[2]);

            xmlFree(prop[0]);
            xmlFree(prop[1]);
            xmlFree(prop[2]);

            return new Material(c, f1, f2);
        }

        cur = cur->next;
    }

    return new Material(new Color(1.0, 1.0, 1.0), 0.0, 0.0);
}

Light *XMLSceneParser::parse_pointlight(xmlNodePtr node)
{
    Color *c;
    float f1, f2, f3, f4;
    Vertex *v;

    xmlChar *prop[6];

    prop[0] = xmlGetProp(node, (const xmlChar *) "position");
    prop[1] = xmlGetProp(node, (const xmlChar *) "intensity");
    prop[2] = xmlGetProp(node, (const xmlChar *) "color");
    prop[3] = xmlGetProp(node, (const xmlChar *) "att0");
    prop[4] = xmlGetProp(node, (const xmlChar *) "att1");
    prop[5] = xmlGetProp(node, (const xmlChar *) "att2");

    v = parse_vertex(prop[0]);
    f1 = parse_float(prop[1]);
    c = parse_color(prop[2]);
    f2 = parse_float(prop[3]);
    f3 = parse_float(prop[4]);
    f4 = parse_float(prop[5]);

    xmlFree(prop[0]);
    xmlFree(prop[1]);
    xmlFree(prop[2]);
    xmlFree(prop[3]);
    xmlFree(prop[4]);
    xmlFree(prop[5]);

    return (Light *) (new PointLight(v, f1, c, f2, f3, f4));
}

/*
 * Parses an xmlChar string of the form "(%f, %f, %f)" to a new
 * corresponding Vertex. If a NULL string is supplied, it returns a
 * default (the origin).
 */
Vertex *XMLSceneParser::parse_vertex(xmlChar *str)
{
    float x, y, z;

    if (str == NULL)
    {
        return new Vertex(0.0, 0.0, 0.0);
    }
    else
    {
        sscanf((char *) str, "(%f, %f, %f)", &x, &y, &z);
        return new Vertex(x, y, z);
    }
}

/*
 * Parses a viewport node, returning a valid Viewport if no error
 * occurs, NULL otherwise.
 */
Viewport *XMLSceneParser::parse_viewport(xmlNodePtr node)
{
    xmlNodePtr cur = node;
    xmlChar *prop;
    Vertex *topleft = NULL, *topright = NULL, *bottomleft = NULL;

    while (cur != NULL)
    {
        if (xmlStrcmp(cur->name, (const xmlChar *) "topleft") == 0)
        {
            prop = xmlGetProp(cur, (const xmlChar *) "position");
            topleft = parse_vertex(prop);
            xmlFree(prop);
        }
        else if (xmlStrcmp(cur->name, (const xmlChar *) "topright") == 0)
        {
            prop = xmlGetProp(cur, (const xmlChar *) "position");
            topright = parse_vertex(prop);
            xmlFree(prop);
        }
        else if (xmlStrcmp(cur->name, (const xmlChar *) "bottomleft") == 0)
        {
            prop = xmlGetProp(cur, (const xmlChar *) "position");
            bottomleft = parse_vertex(prop);
            xmlFree(prop);
        }

        cur = cur->next;
    }

    if ((topleft == NULL) || (topright == NULL) || (bottomleft == NULL))
    {
        return NULL;
    }
    else
    {
        return new Viewport(topleft, topright, bottomleft);
    }
}
