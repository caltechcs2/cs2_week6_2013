#ifndef __STATESTACK_H__
#define __STATESTACK_H__

#include "GameState.h"

class GameState;

typedef enum state_command {
    POP,
    PUSH,
    QUIT_NOW
} state_command;


// Encapsulates a command to the state stack manager,
// including a pointer to the state to push if necessary.
struct StateStackCommand {
    state_command cmd;
    GameState * new_state;
};

#endif
