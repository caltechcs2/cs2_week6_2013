/****************************************************************************
*
* RaytracerGameState.cpp
* State that renders a 3D scene using a raytracer.
*
* Original code copyright (c) 2012 Ellen Price <eprice@caltech.edu>
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#include "RaytracerGameState.h"

#define PHYSICS_DELAY   (5000)

#define STATUS_X        (5)
#define STATUS_Y        (5)

#define XML_SCENE_FILE  ("scene1.xml")

using namespace std;

extern GameState *top_state;

/*
 * Initializes the GameState.
 */
RaytracerGameState::RaytracerGameState(bool multithreaded)
{
    is_multithreaded = multithreaded;

    /* Initialize the frame count. */
    framecount = 0;
    strcpy(status, "FPS: Waiting for data...");

    /* We use a mutex for the keypress handler... Don't ask. */
    keypress = 0;
    pthread_mutex_init(&keypress_mutex, NULL);
}

/*
 * Deinitializes the GameState.
 */
RaytracerGameState::~RaytracerGameState()
{
    if (world)
        delete world;

    if (shader)
        delete shader;

    if (raytracer)
        delete raytracer;

    pthread_mutex_destroy(&keypress_mutex);
}

void RaytracerGameState::do_physics_proc()
{
    ((RaytracerGameState *) top_state)->physics_step();
}

void RaytracerGameState::do_render_proc()
{
    ((RaytracerGameState *) top_state)->render();
}

/*
 * Runs the GameState.
 */
StateStackCommand *RaytracerGameState::run()
{
    int ret = 2;
    XMLSceneParser *sp;
    StateStackCommand *retc = new StateStackCommand();

    /* Initialize the world and scene. */
    world = new World();
    sp = new XMLSceneParser();

    if (sp->load_scene(world, (char *) XML_SCENE_FILE) == -1)
    {
        /* An error occurred loading the scene, so exit early and print
         * an error message to the terminal. */
        printf("Error: Scene file %s failed to load!\n", XML_SCENE_FILE);
        delete sp;

        retc->cmd = POP;
        retc->new_state = NULL;
        return retc;
    }

    delete sp;

    /* Link the viewport to the GameState's buffer. */
    Viewport *vp = world->get_viewport();
    vp->set_bitmap(doublebuf);

    /* Create and initialze the Shader. */
    shader = new Shader();
    shader->set_ambient_coefficient(0.2);
    shader->set_diffuse_coefficient(0.8);
    shader->set_specular_coefficients(0.5, 8.0);

    /* Create and initialize the Raytracer. */
    if (is_multithreaded)
        raytracer = (RaytracerBase *) new RaytracerMultithreaded(world, shader);
    else
        raytracer = (RaytracerBase *) new RaytracerSinglethreaded(world, shader);

#ifndef VALGRIND
    install_int(RaytracerGameState::do_physics_proc, PHYSICS_DELAY);

    while(true)
    {
        pthread_mutex_lock(&keypress_mutex);

        if (keypress == 'q')
            break;

        pthread_mutex_unlock(&keypress_mutex);

        render();
        rest(5);
    }

    remove_int(RaytracerGameState::do_physics_proc);
#else
    render();
#endif

    switch(ret)
    {
        case 2:
        default:
            retc->cmd = POP;
            retc->new_state = NULL;
    }

    return retc;
}

/*
 * Runs a step of the physics layer. This GameState doesn't use any
 * physics, so we'll use physics_step to keep up with the frame rate.
 */
void RaytracerGameState::physics_step()
{
    float fps = (float) framecount / (float) (PHYSICS_DELAY / 1000);
    sprintf(status, "FPS: %f", fps);

    framecount = 0;
}

/*
 * Renders everything.
 */
void RaytracerGameState::render()
{
    start_render();
    raytracer->run();
    framecount++;
    textout_ex(doublebuf, font, status, STATUS_X, STATUS_Y,
        makecol(255, 255, 255), -1);
    end_render();
}

/*
 * Saves the ASCII code of the last keypress to `keypress` so it can
 * be handled when we have time.
 */
void RaytracerGameState::keystroke_handler(int key)
{
    pthread_mutex_lock(&keypress_mutex);
    keypress = key & 0xff;
    pthread_mutex_unlock(&keypress_mutex);
}
