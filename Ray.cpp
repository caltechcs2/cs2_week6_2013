#include "Ray.h"

Ray::Ray()
{
    origin = NULL;
    displacement = NULL;
}

Ray::Ray(Vertex *o, Vertex *d)
{
    float mag;

    origin = o;

    mag = sqrtf(d->x * d->x + d->y * d->y + d->z * d->z);
    d->x /= mag;
    d->y /= mag;
    d->z /= mag;

    displacement = d;
}

Ray::~Ray()
{
    if (origin)
        delete origin;

    if (displacement)
        delete displacement;
}

Vertex *Ray::get_origin()
{
    return origin;
}

void Ray::set_origin(Vertex *o)
{
    if (origin)
        delete origin;

    origin = o;
}

Vertex *Ray::get_displacement()
{
    return displacement;
}

void Ray::set_displacement(Vertex *d)
{
    float mag;

    if (displacement)
        delete displacement;

    mag = sqrtf(d->x * d->x + d->y * d->y + d->z * d->z);
    d->x /= mag;
    d->y /= mag;
    d->z /= mag;

    displacement = d;
}
