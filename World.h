#ifndef __WORLD_H__
#define __WORLD_H__

#include <allegro.h>
#include <vector>
#include "Ray.h"
#include "Entity.h"
#include "Light.h"
#include "Viewport.h"
#include "structs.h"

class World
{
public:
    World();
    ~World();

    void set_eye(Vertex *e);
    void set_viewport(Viewport *v);

    Vertex *get_eye();
    Viewport *get_viewport();

    void add_entity(Entity *e);
    void add_light(Light *l);

    vector<Entity *> get_entity_vector();
    vector<Light *> get_light_vector();

    void get_eye_viewport_ray(int x, int y, Ray **ray);

    int get_viewport_height();
    int get_viewport_width();

private:
    vector<Entity *> entities;
    vector<Light *> lights;
    Vertex *eye;
    Viewport *viewport;
};

#endif
