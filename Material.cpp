#include "Material.h"

Material::Material(Color *c, float reflect, float refract)
{
    color = c;
    reflectivity = reflect;
    refractivity = refract;
}

Material::~Material()
{
    delete color;
}

Color *Material::get_color()
{
    return color;
}

float Material::get_reflectivity()
{
    return reflectivity;
}

float Material::get_refractivity()
{
    return refractivity;
}
