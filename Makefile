CC = g++
CFLAGS = -Wall -ansi -pedantic -ggdb `allegro-config --cppflags` `xml2-config --cflags`
LIBS = -lm `allegro-config --libs` `xml2-config --libs` -lpthread
OBJS = GameState.o main.o MainMenuGameState.o Material.o \
	PointLight.o Ray.o RaytracerGameState.o RaytracerMultithreaded.o \
	RaytracerSinglethreaded.o Shader.o Sphere.o Viewport.o \
	World.o XMLSceneParser.o
APPNAME = RayTracer

all: $(APPNAME) reader

$(APPNAME): $(OBJS)
	$(CC) $(OBJS) $(LIBS) -o $(APPNAME)

reader.o: reader.cpp
	$(CC) $(CFLAGS) -c reader.cpp

GameState.o: GameState.cpp GameState.h StateStack.h
	$(CC) $(CFLAGS) -c GameState.cpp

main.o: main.cpp main.h StateStack.h MainMenuGameState.h
	$(CC) $(CFLAGS) -c main.cpp

MainMenuGameState.o: MainMenuGameState.cpp MainMenuGameState.h GameState.h \
	StateStack.h
	$(CC) $(CFLAGS) -c MainMenuGameState.cpp

Material.o: Material.cpp Material.h structs.h
	$(CC) $(CFLAGS) -c Material.cpp

Plane.o: Plane.cpp Plane.h Entity.h Ray.h Material.h structs.h
	$(CC) $(CFLAGS) -c Plane.cpp

PointLight.o: PointLight.cpp PointLight.h Light.h structs.h
	$(CC) $(CFLAGS) -c PointLight.cpp

Ray.o: Ray.cpp Ray.h structs.h
	$(CC) $(CFLAGS) -c Ray.cpp

RaytracerGameState.o: RaytracerGameState.cpp RaytracerGameState.h \
	GameState.h StateStack.h XMLSceneParser.h RaytracerBase.h \
	RaytracerSinglethreaded.h RaytracerMultithreaded.h Shader.h World.h \
	structs.h
	$(CC) $(CFLAGS) -c RaytracerGameState.cpp

RaytracerMultithreaded.o: RaytracerMultithreaded.cpp RaytracerMultithreaded.h \
	RaytracerBase.h World.h Thread.h
	$(CC) $(CFLAGS) -c RaytracerMultithreaded.cpp

RaytracerSinglethreaded.o: RaytracerSinglethreaded.cpp RaytracerSinglethreaded.h \
	RaytracerBase.h Shader.h World.h
	$(CC) $(CFLAGS) -c RaytracerSinglethreaded.cpp

Shader.o: Shader.cpp Shader.h Ray.h structs.h
	$(CC) $(CFLAGS) -c Shader.cpp

Sphere.o: Sphere.cpp Sphere.h Entity.h Ray.h structs.h
	$(CC) $(CFLAGS) -c Sphere.cpp

Viewport.o: Viewport.cpp Viewport.h structs.h
	$(CC) $(CFLAGS) -c Viewport.cpp

World.o: World.cpp World.h Ray.h Entity.h Light.h Viewport.h structs.h
	$(CC) $(CFLAGS) -c World.cpp

XMLSceneParser.o: XMLSceneParser.cpp XMLSceneParser.h World.h Entity.h \
	Sphere.h Light.h PointLight.h structs.h
	$(CC) $(CFLAGS) -c XMLSceneParser.cpp

clean:
	rm -f *.o $(APPNAME)

memcheck: $(OBJS)
	$(CC) $(CFLAGS) -c -DVALGRIND RaytracerGameState.cpp
	$(CC) $(OBJS) $(LIBS) -o $(APPNAME)
	valgrind --tool=memcheck --leak-check=full ./$(APPNAME) 2>&1 | tee memcheck.full

callgrind: $(OBJS)
	$(CC) $(CFLAGS) -c -DVALGRIND RaytracerGameState.cpp
	$(CC) $(OBJS) $(LIBS) -o $(APPNAME)
	valgrind --tool=callgrind ./$(APPNAME)
