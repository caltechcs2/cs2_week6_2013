#ifndef __STRUCTS_H__
#define __STRUCTS_H__

#include <math.h>

struct Color
{
    Color() { r = 0.0; g = 0.0; b = 0.0; }
    Color(float red, float green, float blue) { r = red; g = green; b = blue; }
    float r, g, b;
};

struct Vertex
{
    Vertex() { }
    Vertex(Vertex *v) { x = v->x; y = v->y; z = v->z; }
    Vertex(float i, float j, float k) { x = i; y = j; z = k; }

    static float dist_squared(Vertex *v1, Vertex *v2)
    {
        float dx = v2->x - v1->x;
        float dy = v2->y - v1->y;
        float dz = v2->z - v1->z;

        return dx * dx + dy * dy + dz * dz;
    }

    float x, y, z;
};

struct Matrix
{
    float x11, x12, x13, x14;
    float x21, x22, x23, x24;
    float x31, x32, x33, x34;
    float x41, x42, x43, x44;

    float det;
};

struct TranslationMatrix : Matrix
{
    TranslationMatrix(Vertex *v)
    {
        x11 = x22 = x33 = x44 = 1.0;
        x12 = x13 = x21 = x23 = x31 = x32 = x41 = x42 = x43 = 0.0;

        x14 = v->x;
        x24 = v->y;
        x34 = v->z;
    }

    void multiply_inverse_by_vertex(Vertex *v, Vertex *dest)
    {
        dest->x = v->x - x14;
        dest->y = v->y - x24;
        dest->z = v->z - x34;
    }

    void multiply_by_vertex(Vertex *v, Vertex *dest)
    {
        dest->x = v->x + x14;
        dest->y = v->y + x24;
        dest->z = v->z + x34;
    }
};

struct ScalingMatrix : Matrix
{
    ScalingMatrix(float sx, float sy, float sz)
    {
        x44 = 1.0;
        x12 = x13 = x14 = x21 = x23 = x24 = x31 = x32 = x34 = 0.0;
        x41 = x42 = x43 = 0.0;

        x11 = sx;
        x22 = sy;
        x33 = sz;
    }

    void multiply_by_vertex(Vertex *v, Vertex *dest)
    {
        dest->x = v->x * x11;
        dest->y = v->y * x22;
        dest->z = v->z * x33;
    }

    void multiply_inverse_by_vertex(Vertex *v, Vertex *dest)
    {
        dest->x = v->x / x11;
        dest->y = v->y / x22;
        dest->z = v->z / x33;
    }
};

#endif
