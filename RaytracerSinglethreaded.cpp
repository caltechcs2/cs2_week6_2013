#include "RaytracerSinglethreaded.h"

/* This constant can be tuned to meet specific project needs; the only
 * requirement is that it be small compared to the scale of the
 * Entities. It is necessary to prevent shadow rounding errors. */
#define EPSILON     (5.0)

RaytracerSinglethreaded::RaytracerSinglethreaded(World *w, Shader *s)
{
    world = w;
    shader = s;
}

RaytracerSinglethreaded::~RaytracerSinglethreaded()
{

}

void RaytracerSinglethreaded::run()
{
    Ray *init_ray = new Ray();
    Color *col;
    Vertex *eye, *pixel;
    Viewport *viewport = world->get_viewport();

    /* The origin of the initial ray is always the viewer's eye. */
    eye = world->get_eye();
    init_ray->set_origin(new Vertex(eye));

    int i, j;
    int height = viewport->get_height();
    int width = viewport->get_width();

    for (i = 0; i < width; i++)
    {
        for (j = 0; j < height; j++)
        {
            /* The displacement of the ray is the pixel's coordinates
             * minus the eye's coordinates. */
            viewport->map_pixel_to_vertex(i, j, &pixel);
            init_ray->set_displacement(new Vertex(pixel->x - eye->x,
                pixel->y - eye->y, pixel->z - eye->z));
            delete pixel;

            /* Trace this ray. */
            trace(init_ray, &col);

            /* Color the pixel. */
            viewport->color_pixel(i, j, col);
            delete col;
        }
    }

    delete init_ray;
}

void RaytracerSinglethreaded::trace(Ray *ray, Color **color)
{
    unsigned int i;
    Entity *entity = NULL;
    Ray *to_light, *normal;
    Vertex *lp, *no;
    vector<Light *> lights = world->get_light_vector();

    *color = new Color(0.0, 0.0, 0.0);

    if (get_closest_entity_intersection(ray, &normal, &entity))
    {
        /* This ray intersects an Entity, so we need to determine the
         * color of the corresponding pixel. */
        for (i = 0; i < lights.size(); i++)
        {
            lp = lights[i]->get_position();
            no = normal->get_origin();

            to_light = new Ray(new Vertex(no),
                new Vertex(lp->x - no->x, lp->y - no->y, lp->z - no->z));

            if (!light_blocked(to_light, lights[i], entity))
            {
                shader->shade_from_single_light(lights[i], entity, ray,
                    to_light, normal, *color);
            }

            delete to_light;
        }

        shader->shade_ambient(entity, *color);

        delete normal;
    }
}

/*
 * Returns true if `ray` intersects an Entity in the World, false
 * otherwise. If there is one or more intersection, `closest_intersect`
 * and `closest_entity` will contain pointers to the point of
 * intersection and the entity itself, respectively.
 */
bool RaytracerSinglethreaded::get_closest_entity_intersection(Ray *ray,
Ray **normal, Entity **entity)
{
    unsigned int i;
    Ray *temp;
    vector<Entity *> entities = world->get_entity_vector();

    *normal = NULL;
    *entity = NULL;

    for (i = 0; i < entities.size(); i++)
    {
        if (entities[i]->does_intersect(ray, &temp))
        {
            if (!(*normal))
            {
                *normal = temp;
                *entity = entities[i];
            }
            else if (Vertex::dist_squared(ray->get_origin(), temp->get_origin()) <
                Vertex::dist_squared(ray->get_origin(), (*normal)->get_origin()))
            {
                delete *normal;
                *normal = temp;
                *entity = entities[i];
            }
            else
            {
                delete temp;
            }
        }
    }

    if (*normal)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
 * Returns true if `ray`, which defines a ray from `entity` to `light`,
 * is blocked by any Entity in the World, false otherwise. `entity` is
 * required because shadow rounding error correction is generally only
 * necessary for the Entity of origin, so we don't waste time with it
 * otherwise.
 */
bool RaytracerSinglethreaded::light_blocked(Ray *ray, Light *light,
Entity *entity)
{
    unsigned int i;
    Ray *normal;
    Vertex *o, *d;
    vector<Entity *> entities = world->get_entity_vector();

    for (i = 0; i < entities.size(); i++)
    {
        if (entities[i] == entity)
        {
            /* We'll need to twitch the origin of the ray along the
             * Entity normal vector. For now we'll just add the normal
             * vector, scaled by EPSILON, to the original ray's origin. */
            o = ray->get_origin();
            d = ray->get_displacement();

            o->x += d->x * EPSILON;
            o->y += d->y * EPSILON;
            o->z += d->z * EPSILON;
        }

        if (entities[i]->does_intersect(ray, &normal))
        {
            delete normal;
            return true;
        }
    }

    return false;
}
