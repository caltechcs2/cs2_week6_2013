#include "Shader.h"

Shader::Shader()
{
    ka = 0.0;
    kd = 0.0;
    ks = 0.0;
    n = 0.0;
}

Shader::~Shader()
{

}

void Shader::shade_from_single_light(Light *light, Entity *entity, Ray *ray,
Ray *to_light, Ray *normal, Color *color)
{
    Ray *to_light_reflect, *to_viewer;
    Vertex *n, *l, v;
    float dot;

    float intensity = light->get_intensity_at_point(to_light->get_origin());
    Color *light_color = light->get_color();
    Color *entity_color = entity->get_material()->get_color();

    add_diffuse_color(intensity, light_color, entity_color, to_light,
        normal, color);

    dot = Ray::dot_product(normal, to_light);
    l = to_light->get_displacement();
    n = normal->get_displacement();

    v.x = 2.0 * dot * n->x - l->x;
    v.y = 2.0 * dot * n->y - l->y;
    v.z = 2.0 * dot * n->z - l->z;

    to_light_reflect = new Ray(new Vertex(to_light->get_origin()), new Vertex(&v));

    n = ray->get_displacement();
    v.x = -n->x;
    v.y = -n->y;
    v.z = -n->z;

    to_viewer = new Ray(new Vertex(to_light->get_origin()), new Vertex(&v));

    add_specular_color(intensity, light_color, entity_color, to_viewer,
        to_light_reflect, color);

    delete to_light_reflect;
    delete to_viewer;
}

/*
 * Calculates the diffuse lighting component (Lambertian) at a point,
 * given `intensity`, the light intensity at that point, `light_color`,
 * the light's color, `to_light`, a ray from the point to the light, and
 * `normal`, the surface normal at the point. Note: `to_light` and `normal`
 * should be pre-normalized.
 */
void Shader::add_diffuse_color(float intensity, Color *light_color,
Color *entity_color, Ray *to_light, Ray *normal, Color *color)
{
    float dot = Ray::dot_product(to_light, normal);
    dot = 0.0 > dot ? 0.0 : dot;

    color->r += kd * intensity * light_color->r * entity_color->r * dot;
    color->g += kd * intensity * light_color->g * entity_color->g * dot;
    color->b += kd * intensity * light_color->b * entity_color->b * dot;
}

/*
 * Calculates the specular lighting component (Blinn-Phong) at a point,
 * given `intensity`, the light intensity at that point, `light_color`,
 * the light's color, `to_viewer`, a ray from the point to the viewer, and
 * `reflect`, the light reflection ray. Note: `to_viewer` and `reflect`
 * should be pre-normalized.
 */
void Shader::add_specular_color(float intensity, Color *light_color,
Color *entity_color, Ray *to_viewer, Ray *reflect, Color *color)
{
    float dot = Ray::dot_product(to_viewer, reflect);
    dot = 0.0 > dot ? 0.0 : dot;

    color->r += intensity * light_color->r * entity_color->r * powf(dot, n);
    color->g += intensity * light_color->g * entity_color->g * powf(dot, n);
    color->b += intensity * light_color->b * entity_color->b * powf(dot, n);
}

/*
 * Adds the ambient shading component to `color` based on the ambient
 * coefficient and the color of `entity`.
 */
void Shader::shade_ambient(Entity *entity, Color *color)
{
   Color *entity_color = entity->get_material()->get_color();

   color->r += ka * entity_color->r;
   color->g += ka * entity_color->g;
   color->b += ka * entity_color->b;
}

void Shader::set_ambient_coefficient(float ka)
{
    this->ka = ka;
}

void Shader::set_diffuse_coefficient(float kd)
{
    this->kd = kd;
}

void Shader::set_specular_coefficients(float ks, float n)
{
    this->ks = ks;
    this->n = n;
}
