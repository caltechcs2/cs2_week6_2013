#ifndef __RAY_H__
#define __RAY_H__

#include <stdlib.h>
#include "structs.h"

using namespace std;

class Ray
{
public:
    Ray();
    Ray(Vertex *o, Vertex *d);
    ~Ray();

    static float dot_product(Ray *r1, Ray *r2)
    {
        Vertex *v1 = r1->get_displacement();
        Vertex *v2 = r2->get_displacement();

        return v1->x * v2->x + v1->y * v2->y + v1->z * v2->z;
    }

    Vertex *get_origin();
    void set_origin(Vertex *o);

    Vertex *get_displacement();
    void set_displacement(Vertex *d);

private:
    Vertex *origin, *displacement;
};

#endif
