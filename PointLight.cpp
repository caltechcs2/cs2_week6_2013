/****************************************************************************
*
* PointLight.cpp
* Class that defines a point source light in 3D space.
*
* This code copyright (c) 2012 Ellen Price <eprice@caltech.edu>
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#include "PointLight.h"

/*
 * Initializes the PointLight with position `pos`, initial intensity `i`
 * in the range [0.0, 1.0], color `col`, and attenuation constants `a0`,
 * `a1`, and `a2`. The attenuation of the point light will be given by
 *     i / (a0 + a1 * x + a2 * x^2)
 * where `x` is the radial distance from the source.
 */
PointLight::PointLight(Vertex *pos, float i, Color *col, float a0,
float a1, float a2)
{
    position = pos;
    intensity = i;
    color = col;
    att0 = a0;
    att1 = a1;
    att2 = a2;
}

/*
 * Deinitializes the PointLight.
 */
PointLight::~PointLight()
{
    delete position;
    delete color;
}

/*
 * The light intensity falls of with the square of the distance.
 */
float PointLight::get_intensity_at_point(Vertex *p)
{
    float dsq = Vertex::dist_squared(position, p);
    return intensity / (att2 * dsq);
}

/*
 * Returns the position of the light.
 */
Vertex *PointLight::get_position()
{
    return position;
}
