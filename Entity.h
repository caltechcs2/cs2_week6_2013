#ifndef __ENTITY_H__
#define __ENTITY_H__

#include <stdlib.h>
#include "Ray.h"
#include "Material.h"
#include "structs.h"

using namespace std;

class Entity
{
public:
    Entity() { material = NULL; t = NULL; s = NULL; }
    virtual ~Entity()
    {
        delete material;
        if (t) delete t;
        if (s) delete s;
    };

    virtual bool does_intersect(Ray *ray, Ray **normal) = 0;
    Material *get_material() { return material; }

protected:
    Material *material;
    TranslationMatrix *t;
    ScalingMatrix *s;
};

#endif
