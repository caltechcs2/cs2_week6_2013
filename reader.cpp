#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// This file contains the definitions for a Node and a Queue. This code will
// read lines of input until the end of the file is reached (Ctrl-D from the
// terminal), and then it will print out the first NUM_OUTPUT lines of the
// input. If a line is longer than BUFFER_SIZE, it will be treated as
// separate lines (due to the behavior of fgets).

// Size of the buffer for reading input
const int BUFFER_SIZE = 1000;

// Number of lines to output
const int NUM_OUTPUT = 5;

class Node;
class Queue;

// Node in a Queue
class Node
{
  // Used to allow a Queue to access the next pointer
  friend class Queue;

protected:
  // String stored by the Node
  char *string;
  // Next Node in the Queue
  Node *next;

public:
  // Constructor to create a Node with no Node after it
  Node(char *string)
  {
    this->string = string;
    next = NULL;
  }

};

class Queue
{
protected:
  // First Node in the Queue
  Node *head;
  // Last Node in the Queue
  Node *tail;

public:
  // Constructor for an empty Queue
  Queue()
  {
    head = NULL;
    tail = NULL;
  }

  // Inserts a string to the end of the Queue
  void push_back(char *string)
  {
    Node *node = new Node(string);
    if (head == NULL) {
      head = node;
      tail = node;
    }
    else {
      tail->next = node;
      tail = node;
    }
  }

  // Removes a string from the front of the Queue
  char *pop_front()
  {
    if (head == NULL) {
      return NULL;
    }
    Node *temp = head;
    head = head->next;
    return temp->string;
  }

  // Checks if the Queue is empty
  bool is_empty()
  {
    return head == NULL;
  }

};

int main()
{
  // Create a queue to store the strings
  Queue queue = Queue();
  // Create a buffer to hold the input
  char buffer[BUFFER_SIZE];

  int count = 0;
  while (true) {

    // Read a line of input
    fgets(buffer, BUFFER_SIZE, stdin);
    if (feof(stdin)) {
      // No more input
      break;
    }

    // One more string was read
    count++;

    // Copy the input into another string
    size_t len = strlen(buffer);
    char *string = new char[len];
    strncpy(string, buffer, len);

    // Add the string to the Queue
    queue.push_back(string);
  }

  count = 0;
  while (!queue.is_empty() && count < NUM_OUTPUT) {
    // Print the first few lines
    count++;
    printf("String #%d: %s\n", count, queue.pop_front());
  }

  return 0;

}

