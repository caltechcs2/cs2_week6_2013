<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="cs2_assignment.css" rel="stylesheet" type="text/css" />
<title>CS2 Assignment 6: Multithreading</title>
</head>

<body>
<div class="content">

<div class="author">Author: Ellen Price</div>
<h1>CS2 Assignment 6: Multithreading</h1>
<h2>Due Tuesday, January 29, 2013, at 4:00 AM</h2>

<hr />

<h2>Introduction</h2>

<p>This is the CS2 assignment on multithreading and concurrency. In
particular, you will be working with a raytracer.</p>

<p>When finished, please enclose your submission as a ZIP file named 
<span class="code">cs2week6-[NAME].zip</span> or a tar.gz file named 
<span class="code">cs2week6-[NAME].tar.gz</span>, and upload it to the 
Week 6 assignment module on Moodle.</p>

<h2>Assignment Background</h2>

<p>In the CS2 assignment on sorting, you were asked to optimize sorting
algorithms by using Cython instead of Python; this should have proved
to you that compiled languages have the potential to be much faster than
scripting languages when it comes to CPU-intensive tasks. However, some
tasks are prohibitively slow even when implemented in a compiled
language like C++; one example is raytracing, and a common solution
to this problem is multithreading.</p>

<p>We will <i>not</i> be asking you to write a raytracer in this
assignment, but you should understand how the process works in general.
<b>Please note that the images below are shamelessly taken from 
<a href="http://www.cs.unc.edu/~rademach/xroads-RT/RTarticle.html">Ray
Tracing: Graphics for the Masses</a>.</b> How will we represent the real
world in a graphics-friendly way? The most common way (that I've seen,
at least) is by defining a single point for an "eye" (the viewer) and a
plane surface for the viewport (what you see on your screen). Then
a pixel on the viewport is colored according to what a ray from the
eye through that viewport pixel would see. In the "world," we define
objects (spheres, planes, etc.) and lights (which illuminate the
objects but cannot be seen themselves).</p>

<center><img src="images/eye_window.jpg" /></center>

<p>The natural question to ask now is, how do we know what color that
pixel should be at all? Enter raytracing. A naive approach to this
problem would be to define every possible ray extending from every light,
check whether it intersects an object, and, if it does, check if the
reflection will hit the viewport. This is a very bad idea! All the rays
that didn't intersect an object or the viewport will be deleted, so
there was no point in calculating them anyway. A scheme like this
is visualized in the figure below. See all the wasted rays?</p>

<center><img src="images/naive_raytracing.jpg" /></center>

<p>Clearly, this is not the best way to proceed. What if we work the
problem backwards? Trace every ray from the eye through the viewport;
if a ray intersects an object, check to see if light would illuminate
that point and, if so, color the viewport accordingly. All of <i>those</i>
rays must intersect the viewport, which means much less waste (compare
the image below with the previous one). This should give you a basic
idea of how the provided raytracer works. If you're more curious
you can look at
<a href="http://www.cs.cornell.edu/courses/cs4620/2011fa/lectures/08raytracingWeb.pdf">
Cornell's Raytracing Basics</a>.</p>

<center><img src="images/smart_raytracing.jpg" /></center>

<p>I told you that this method of raytracing is faster, and it is.
However, I would be careful about calling it "fast" until you try it
for yourself. For a 600x800 pixel image (the size you will be using),
you must trace a minimum of 480,000 rays from the eye to the
viewport, not to mention determining whether light rays will
illuminate a particular point (for N lights, you would do this N
times for every ray that intersects an object). That's a lot of
computation, and we can improve speed by taking advantage of
multithreading.</p>

<p>Multithreading can be used to break up a process so that parts
of it can execute at the same time; in the best case scenario,
if you have two threads executing at the same time, you could improve
speed by a factor of two (if you are working on a multicore system, that
is). You will see in this assignment that raytracing can be made
significantly faster by running multiple threads at once. However,
raytracing also introduces certain problems, such as race conditions,
which you will be asked to address.</p>

<h2>Prerequisites</h2>

<p><ul>
	<li>g++ 4.6.x+</li>
	<li>liballegro4.2-dev</li>
	<li>libxml2-dev</li>
</ul>

Ask a TA if you need help retrieving these packages, or if these packages 
appear to be missing from the CS cluster.</p>

<h2>Assignment (20 points)</h2>

<p>For this assignment, you will parallelize a provided, singlethreaded
raytracer. <b>Note: While this assignment will be graded out of 20
points, more than 20 points are available.</b></p>

<h3>Part 0: Debugging</h3>

<p><div class="points easy">8</div>In the file reader.cpp, there is an
implementation of a queue. This queue is used to store lines of input
from the terminal, and prints the first few lines after the end of file
is reached (Ctrl-D from the terminal). The code does run, but it
currently has issues with memory. Using valgrind, find and fix all memory
issues in this code.</p>

<h3>Part 1: Raytracing</h3>

<p><b>IMPORTANT:</b> If you are working in the Annenberg lab, please
modify the provided Makefile by replacing the lines</p>

<div class="code">
CFLAGS = -Wall -ansi -pedantic -ggdb `allegro-config --cppflags`
	`xml2-config --cflags`
LIBS = -lm `allegro-config --libs` `xml2-config --libs` -lpthread
</div>

<p>with</p>

<div class="code">
CFLAGS = -Wall -ansi -pedantic -ggdb -I/cs/courses/cs2/allegro-4.4.2/include
	-I/cs/courses/cs2/allegro-4.4.2/build/include `xml2-config --cflags`
LIBS = -lm -L/cs/courses/cs2/allegro-4.4.2/build/lib -lalleg
	`xml2-config --libs` -lpthread
</div>

<p class="hilite">and run
<span class="code">export LD_LIBRARY_PATH=/cs/courses/cs2/allegro-4.4.2/build/lib:$LD_LIBRARY_PATH</span>
once per session (you need to run it again if you log out and log
back in).</p>

<p>First, you should familiarize yourself with the provided raytracer
framework. Classes that it may be useful to be familar with are:</p>

<ul>
	<li><b>Entity:</b> A base class for world objects, which
		determines all the functions such an object must have. The
		only Entity that is currently defined is the <b>Sphere</b>.
		Relevant files are Entity.h, Sphere.h, and Sphere.cpp.</li>
	<li><b>Light:</b> A Light describes a virtual light in world space.
		Remember that lights cannot be seen themselves, but their
		effects can. The only Light that is currently defined is
		the <b>PointLight</b>. Relevant files are Light.h, PointLight.h,
		and PointLight.cpp.</li>
	<li><b>Ray:</b> Defines a geometric ray with an origin (a <b>Vertex</b>)
		and a displacement (also a Vertex). Thus, the endpoint of a Ray
		is just the origin plus the displacement. Relevant files are
		Ray.cpp, Ray.h, and structs.cpp (for Vertex).</li>
</ul>

<p>Classes that you might want to look at (but can generally treat as
blackboxes) are <b>Material</b>, <b>Shader</b>, <b>Viewport</b>,
<b>XMLSceneParser</b>, and <b>World</b>. You will be using the
<b>RaytracerSinglethreaded</b> class (found in RaytracerSinglethreaded.cpp)
to write the <span class="code">run</span> function of the
<b>RaytracerMultithreaded</b> class (found in
RaytracerMultithreaded.cpp). Run the code by doing ./RayTracer at
a terminal. The singlethreaded button will render the provided scene
file (scene1.xml); you should see the same thing if your multithreaded
version is working. As usual, press 'q' to quit the program.</p>

<p><div class="points easy">8</div>Go ahead and open up
RaytracerSinglethreaded.cpp. The function <span class="code">run</span>
calculates a ray through the eye and a single pixel on the viewport; it
calls <span class="code">trace</span> on each of these rays.
<span class="code">trace</span> does all the hard work of determining
the correct color for the pixel and returns it, and
<span class="code">run</span> paints the color on the viewport.</p>

<p>This process can be made faster with multithreading. You've been
given the classes Thread, Mutex, and Semaphore in the file Thread.h.
We can't call a member function from a thread directly, so we
have to wrap it in a non-class function first, like this:</p>

<div class="code">
class Foo
{
	Foo() { }
	~Foo() { }

	void f() { /* Do something interesting here. */ }
};
	
void *f_wrapper(void *arg)
{
	Foo *fptr = (Foo *) arg;
	fptr->f();
}

Foo *f = new Foo();
Thread *t = new Thread();
t->run(f_wrapper, (void *) f);
</div>

<p>The overhead associated with creating a thread is high, so you should
try to create as few threads as possible and give them as much to do
as possible. My implementation of this assignment uses one thread to
render half the image and another one to render the other half. Feel
free to experiment with different methods. You will need at least a
dual-core processor to observe the speedup you will get by using
threads (the frames-per-second counter in the upper left corner of
the screen should also help). The Annenberg lab machines have
demonstrated a 2x speedup when using two threads; you should achieve
similar results (I got 0.6 FPS singlethreaded, 1.2 FPS multithreaded).</p>

<p>The Makefile provided has a <span class="code">callgrind</span>
target that produces a file which you can view with kcachegrind if
you would like. It will tell you how many times each function was
called, how long it took to run, who called it, etc. If your code
is unreasonably slow, this is a good debugging tool. However, if you use
this Makefile target, you will need to <span class="code">make clean</span>
and <span class="code">make all</span> afterwards to revert to the
normal behavior.</p>

<p><div class="points hard">2</div>The Viewport class (defined in Viewport.h
and Viewport.cpp) has a frames rendered counter
(<span class="code">count</span>) that is incremented in a
non-threadsafe way (by design) in the function
<span class="code">color_pixel</span>. Use the Mutex in Thread.h to prevent
the race condition this introduces.</p>

<p><div class="points hard">4</div>There is a
<span class="code">memcheck</span> target in the Makefile for this
assignment that will build a version of the code that renders a
single frame and then exits. Use this to resolve all your memory
leaks. If you use this Makefile target, you will need to
<span class="code">make clean</span> and <span class="code">make all</span>
afterwards to revert to the normal behavior.</p>

<p>If you have any questions about this week's assignment, please contact 
cs2-tas@ugcs.caltech.edu, or show up at any TA's office hours.</p>

</div>
</body>
</html>
