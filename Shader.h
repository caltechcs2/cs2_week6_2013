#ifndef __SHADER_H__
#define __SHADER_H__

#include "Ray.h"
#include "Light.h"
#include "Entity.h"
#include "structs.h"

using namespace std;

class Shader
{
public:
    Shader();
    ~Shader();

    void shade_from_single_light(Light *light, Entity *entity, Ray *ray,
        Ray *to_light, Ray *normal, Color *color);
    void shade_ambient(Entity *entity, Color *color);

    void set_ambient_coefficient(float ka);
    void set_diffuse_coefficient(float kd);
    void set_specular_coefficients(float ks, float n);

private:
    float ka, kd, ks, n;

    void combine_colors(Color *material, Color *ambient,
        Color *diffuse, Color *specular, Color *color);
    void add_diffuse_color(float intensity, Color *light_color,
        Color *entity_color, Ray *to_light, Ray *normal, Color *color);
    void add_specular_color(float intensity, Color *light_color,
        Color *entity_color, Ray *to_viewer, Ray *reflect, Color *color);
};

#endif
